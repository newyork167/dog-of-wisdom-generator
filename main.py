from RandomDogOfWisdom import Random as RandomDogOfWisdom
from DogOfWisdomSequencer import OpenAndSplit as OpenAndSplit


if __name__ == '__main__':
    wav_file_to_parse = "Dog of Wisdom.wav"
    output_folder = OpenAndSplit.split_wave_by_gaps(wav_file_path=wav_file_to_parse, min_silence_len=200, silence_thresh=-20)
    for _ in range(10):
        RandomDogOfWisdom.merge_random_chunks(wav_file_chunks_path=output_folder, random_length=100)
