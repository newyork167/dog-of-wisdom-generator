import os
from pathlib import Path

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


def create_path_if_not_exist(directory):
    path = Path(directory)
    if not path.exists():
        path.mkdir(parents=True, exist_ok=True)
    return str(path)


def file_exists_at_path(path):
    return os.path.isfile(path)
