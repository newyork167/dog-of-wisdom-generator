import glob
import wave
from pydub import AudioSegment
from pydub.silence import split_on_silence
import ntpath
import Utilities
import os
from Utilities import create_path_if_not_exist, file_exists_at_path


def split_wave_by_gaps(wav_file_path, output_path="{root}/output".format(root=Utilities.ROOT_DIR), min_silence_len=170, silence_thresh=-24):
    """ Splits audio wave by gaps of silence """
    wave_file_name = ntpath.basename(wav_file_path).split('.')[0]
    sound_file = AudioSegment.from_wav(wav_file_path)
    audio_chunks = split_on_silence(sound_file, min_silence_len=min_silence_len, silence_thresh=silence_thresh)
    output_folder = "{output_path}/{output_folder}".format(output_path=output_path, output_folder=wave_file_name)

    # Remove all previous chunks
    [os.remove(f) for f in glob.glob('{output_folder}/*.wav'.format(output_folder=output_folder))]

    create_path_if_not_exist(output_folder)

    for i, chunk in enumerate(audio_chunks):
        write_chunk_to_file(chunk=chunk, output_folder=output_folder, chunk_num=i)

    return output_folder


def write_chunk_to_file(chunk, chunk_num, output_folder):
    """ Writes each chunk to a unique file """
    out_file = "{output_folder}/chunk{chunk_num}.wav".format(output_folder=output_folder, chunk_num=chunk_num)
    print("exporting {}".format(out_file))
    chunk.export(out_file, format="wav")


def open_wave_file(wav_file_path):
    """ Opens a wave file and returns file handle """
    if not file_exists_at_path(path=wav_file_path):
        raise Exception("File does not exist!")

    wav_file = wave.open(wav_file_path)

    if not wav_file:
        raise Exception("Could Not Open File")

    return wav_file
