import glob
import random
import wave
import Utilities

RANDOM_DIR_PATH = 'output/random/'


def get_next_random_num():
    """ Gets next random file chunk number """
    next_random_num = 0

    random_files_list = sorted([int(f.split('_')[-1].split('.')[0]) for f in glob.glob('{root}/{random_dir_path}/random_*.wav'.format(root=Utilities.ROOT_DIR, random_dir_path=RANDOM_DIR_PATH))])
    if len(random_files_list):
        next_random_num = random_files_list[-1] + 1

    return next_random_num


def get_randomized_infiles(wav_file_chunks_path):
    """ Gets all globbed random files """
    infiles = glob.glob("{wav_file_chunks_path}/*.wav".format(wav_file_chunks_path=wav_file_chunks_path))
    random.shuffle(infiles)
    return infiles


def write_random_output(files_to_include, outfile):
    """ Writes randomized DoW audio file """
    print("\tWriting to: {}".format(outfile))
    data = []
    for infile in files_to_include:
        w = wave.open(infile, 'rb')
        data.append([w.getparams(), w.readframes(w.getnframes())])
        w.close()

    output = wave.open(outfile, 'wb')
    output.setparams(data[0][0])

    for d in data:
        output.writeframes(d[1])
    output.close()


def setup_random_outfile():
    """ Returns file handle to next random out file """
    next_random_num = get_next_random_num()
    outfile = "{root}/{random_dir_path}/random_{random_num}.wav".format(root=Utilities.ROOT_DIR, random_num=next_random_num, random_dir_path=RANDOM_DIR_PATH)
    open(outfile, 'a').close()

    return outfile


def merge_random_chunks(wav_file_chunks_path, random_length=0):
    """ Merges DoW base chunks randomly """
    print("Randomly Merging Files From: {}".format(wav_file_chunks_path))

    Utilities.create_path_if_not_exist("{root}/{random_dir_path}".format(root=Utilities.ROOT_DIR, random_dir_path=RANDOM_DIR_PATH))

    infiles = get_randomized_infiles(wav_file_chunks_path=wav_file_chunks_path)
    outfile = setup_random_outfile()

    write_random_output(
        files_to_include=infiles[:random_length if random_length > 0 else len(infiles)],
        outfile=outfile
    )
